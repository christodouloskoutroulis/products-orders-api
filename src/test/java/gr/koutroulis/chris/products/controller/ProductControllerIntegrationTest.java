package gr.koutroulis.chris.products.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.model.enums.ProductType;
import gr.koutroulis.chris.products.util.DatabaseInitializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ProductControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper jsonMapper;
    
    @Autowired
    DatabaseInitializer databaseInitializer;

    @Before
    public void setup() {
        databaseInitializer.initialize();
        jsonMapper = new ObjectMapper();        
    }

    @Test
    public void getProductsShouldReturnProductsWithOk() throws Exception {
        //Arrange
        List<Product> expectedProducts = createExpectedProducts();
        String expectedJsonResponse = jsonMapper.writeValueAsString(expectedProducts);


        //Act
        mockMvc.perform(MockMvcRequestBuilders.get("/products")
                .accept(MediaType.APPLICATION_JSON))
                //Assert
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(expectedJsonResponse));
    }

    @Test
    public void addProductShouldReturnHTTPCreatedWithExpectedProduct() throws Exception {
        //Arrange
        Product givenProduct = createExpectedProducts().get(0);
        String givenJsonRequestBody = jsonMapper.writeValueAsString(givenProduct);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(givenJsonRequestBody)
                .accept(MediaType.APPLICATION_JSON))
                //Assert
                .andExpect(MockMvcResultMatchers.status()
                        .isCreated());
    }


    @Test
    public void addProductShouldReturnHTTPBadRequestForProductWithNullName() throws Exception {
        //Arrange
        Product givenProduct = createExpectedProducts().get(0);
        givenProduct.setName(null);
        String givenJsonRequestBody = jsonMapper.writeValueAsString(givenProduct);
        

        //Act
        mockMvc.perform(MockMvcRequestBuilders.post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(givenJsonRequestBody)
                .accept(MediaType.APPLICATION_JSON))
                //Assert
                .andExpect(MockMvcResultMatchers.status()
                        .isBadRequest());
    }

    private List<Product> createExpectedProducts() {
        List<Product> productList = new ArrayList<>();

        Product product1 = new Product();
        product1.setId(1L);
        product1.setName("Smartphone");
        product1.setType(ProductType.DEVICE);
        product1.setDescription("The new flagship from Swisscom.");

        Product product2 = new Product();
        product2.setId(2L);
        product2.setName("Business pack");
        product2.setType(ProductType.CONTRACT);
        product2.setDescription("All a businessman needs froma communication pack.");

        Product product3 = new Product();
        product3.setId(3L);
        product3.setName("Ethernet cable");
        product3.setType(ProductType.MISCELLANEOUS);
        product3.setDescription("10m of ethernet cable.");

        productList.add(product1);
        productList.add(product2);
        productList.add(product3);

        return productList;
    }

}
