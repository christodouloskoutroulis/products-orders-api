package gr.koutroulis.chris.products.service;

import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.model.Stock;
import gr.koutroulis.chris.products.model.enums.ProductType;
import gr.koutroulis.chris.products.repository.ProductRepository;
import gr.koutroulis.chris.products.repository.StockRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.swing.text.html.Option;
import java.util.Optional;

public class StockServiceTest {
    private StockRepository stockRepositoryMock;
    private ProductRepository productRepositoryMock;
    private StockService stockService;

    @BeforeEach
    public void setup() {
        productRepositoryMock = Mockito.mock(ProductRepository.class);
        stockRepositoryMock = Mockito.mock(StockRepository.class);
        stockService = new StockService(stockRepositoryMock, productRepositoryMock);
    }

    @Test
    public void addStockShouldReturnStock() throws Exception {
        //Arrange
        Stock aStock = createStockWithoutId();
        Stock expectedStock = createStockWithId();
        Product expectedProduct = createProduct();
        Long expectedProductId = expectedProduct.getId();

        //Stub
        Mockito.when(productRepositoryMock.findById(expectedProductId))
                .thenReturn(Optional.of(expectedProduct));
        Mockito.when(stockRepositoryMock.findByProductId(expectedProductId))
                .thenReturn(null);
        Mockito.when(stockRepositoryMock.save(aStock))
                .thenReturn(expectedStock);

        //Act
        Optional<Stock> actualStockOptional = stockService.addStock(aStock);


        //Assert
        Assert.assertEquals("The stock was not the one that was expected.",
                expectedStock, actualStockOptional.get());

        Mockito.verify(productRepositoryMock, Mockito.times(1))
                .findById(expectedProductId);
        Mockito.verify(stockRepositoryMock, Mockito.times(1))
                .findByProductId(expectedProductId);
        Mockito.verify(stockRepositoryMock, Mockito.times(1))
                .save(aStock);
    }


    @Test
    public void addStockShouldReturnNullForNonExistingProduct() throws Exception {
        //Arrange
        Stock aStock = createStockWithoutId();
        Product givenStockProduct = createProduct();
        Long givenStockProductId = givenStockProduct.getId();
        Optional<Product> nonExistingProduct = Optional.empty();
        Optional<Stock> expectedStockOptional = Optional.empty();

        //Stub
        Mockito.when(productRepositoryMock.findById(givenStockProductId))
                .thenReturn(nonExistingProduct);

        //Act
        Optional<Stock> actualStockOptional = stockService.addStock(aStock);


        //Assert
        Assert.assertEquals("The stock optional was expected to be empty.", expectedStockOptional, actualStockOptional);

        Mockito.verify(productRepositoryMock, Mockito.times(1))
                .findById(givenStockProductId);
    }

    @Test
    public void addStockShouldReturnNullForWhenStockExistsForProduct() throws Exception {
        //Arrange
        Stock aStock = createStockWithoutId();
        Product expectedProduct = createProduct();
        Long expectedProductId = expectedProduct.getId();
        Stock existingStockForProduct = new Stock();
        Optional<Stock> expectedStockOptional = Optional.empty();

        //Stub
        Mockito.when(productRepositoryMock.findById(expectedProductId))
                .thenReturn(Optional.of(expectedProduct));
        Mockito.when(stockRepositoryMock.findByProductId(expectedProductId))
                .thenReturn(existingStockForProduct);

        //Act
        Optional<Stock> actualStockOptional = stockService.addStock(aStock);


        //Assert
        Assert.assertEquals("The stock optional was expected to be empty.", expectedStockOptional, actualStockOptional);

        Mockito.verify(productRepositoryMock, Mockito.times(1))
                .findById(expectedProductId);
        Mockito.verify(stockRepositoryMock, Mockito.times(1))
                .findByProductId(expectedProductId);
    }


    private Stock createStockWithoutId() {
        Stock stock = new Stock();
        stock.setProduct(createProduct());
        stock.setQuantity(100);
        return stock;
    }

    private Product createProduct() {
        Product product = new Product();
        product.setId(2L);
        product.setName("Smartphone");
        product.setType(ProductType.DEVICE);
        product.setDescription("The new flagship from Swisscom.");
        return product;
    }

    private Stock createStockWithId() {
        Stock stock = createStockWithoutId();
        stock.setId(1L);
        return stock;
    }


}
