package gr.koutroulis.chris.products.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Objects;


@Entity
public class Stock {
    @Id
    @GeneratedValue
    @Column(name = "stock_id")
    private Long id;

    @NotNull
    @Min(0)
    @Column(nullable = false)
    private Integer quantity;

    @Min(0)
    private Integer maximumQuantity;

    @Min(0)
    private Integer lowQuantityAlert;

    @NotNull
    @OneToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getMaximumQuantity() {
        return maximumQuantity;
    }

    public void setMaximumQuantity(Integer maximumQuantity) {
        this.maximumQuantity = maximumQuantity;
    }

    public Integer getLowQuantityAlert() {
        return lowQuantityAlert;
    }

    public void setLowQuantityAlert(Integer lowQuantityAlert) {
        this.lowQuantityAlert = lowQuantityAlert;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return id.equals(stock.id) &&
                quantity.equals(stock.quantity) &&
                Objects.equals(maximumQuantity, stock.maximumQuantity) &&
                Objects.equals(lowQuantityAlert, stock.lowQuantityAlert) &&
                product.equals(stock.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, quantity, maximumQuantity, lowQuantityAlert, product);
    }
}
