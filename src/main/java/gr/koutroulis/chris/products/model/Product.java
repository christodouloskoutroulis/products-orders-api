package gr.koutroulis.chris.products.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gr.koutroulis.chris.products.model.enums.ProductType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
@Entity
public class Product {
    @Id
    @GeneratedValue
    @Column(name = "product_id")
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ProductType type;


    @NotNull
    @Column(nullable = false)
    private String description;

    @OneToOne(cascade = CascadeType.REMOVE, mappedBy = "product")    
    @JsonIgnore
    private Stock stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id.equals(product.id) &&
                name.equals(product.name) &&
                type == product.type &&
                Objects.equals(description, product.description) &&
                Objects.equals(stock, product.stock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, description, stock);
    }
}
