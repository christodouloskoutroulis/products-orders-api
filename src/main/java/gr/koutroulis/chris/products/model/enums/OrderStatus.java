package gr.koutroulis.chris.products.model.enums;

public enum OrderStatus {
    RECEIVED, RUNNING, SENT, DELIVERED, CANCELED, RETURNED
}
