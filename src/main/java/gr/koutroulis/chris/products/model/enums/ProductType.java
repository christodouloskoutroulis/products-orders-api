package gr.koutroulis.chris.products.model.enums;

public enum ProductType {
    DEVICE, CONTRACT, MISCELLANEOUS
}
