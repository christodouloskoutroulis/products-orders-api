package gr.koutroulis.chris.products.advice;

import gr.koutroulis.chris.products.dto.ErrorResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionControllerAdvice {

    private final static Logger logger = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

    @ExceptionHandler()
    public ResponseEntity<ErrorResponseDTO> handleException(Exception ex) {
        logger.error(ex.getMessage(), ex);

        if (ex instanceof DataIntegrityViolationException) {
            ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(ex.getMessage());
            return ResponseEntity.badRequest()
                    .body(errorResponseDTO);
        }

        if (ex instanceof HttpMessageNotReadableException) {
            ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(ex.getMessage());
            return ResponseEntity.badRequest()
                    .body(errorResponseDTO);
        }

        if (ex.getCause().getCause() instanceof ConstraintViolationException) {
            ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(ex.getCause().getCause().getMessage());
            return ResponseEntity.badRequest()
                    .body(errorResponseDTO);
        }

        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(errorResponseDTO);
    }
}
