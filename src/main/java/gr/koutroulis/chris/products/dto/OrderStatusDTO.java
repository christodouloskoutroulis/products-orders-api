package gr.koutroulis.chris.products.dto;

import gr.koutroulis.chris.products.model.enums.OrderStatus;

public class OrderStatusDTO {
    private OrderStatus status;

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }
}
