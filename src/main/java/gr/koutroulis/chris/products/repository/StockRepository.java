package gr.koutroulis.chris.products.repository;

import gr.koutroulis.chris.products.model.Stock;
import org.springframework.data.repository.CrudRepository;

public interface StockRepository extends CrudRepository<Stock, Long> {
    Stock findByProductId(Long productId);
}
