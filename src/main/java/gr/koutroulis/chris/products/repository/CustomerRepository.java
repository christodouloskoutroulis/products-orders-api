package gr.koutroulis.chris.products.repository;

import gr.koutroulis.chris.products.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
