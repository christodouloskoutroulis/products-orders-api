package gr.koutroulis.chris.products.repository;

import gr.koutroulis.chris.products.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
