package gr.koutroulis.chris.products.repository;

import gr.koutroulis.chris.products.model.CustomerOrder;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<CustomerOrder, Long> {
}
