package gr.koutroulis.chris.products.service;

import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Optional<Product> findProduct(Long id) {
        return productRepository.findById(id);
    }

    public Product updateProduct(Product product) {
        return productRepository.save(product);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    public List<Product> getAllProducts() {
        List<Product> productList = new LinkedList<>();
        productRepository.findAll()
                .forEach(productList::add);

        return productList;
    }
}
