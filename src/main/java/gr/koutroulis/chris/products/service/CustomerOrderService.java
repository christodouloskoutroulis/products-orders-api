package gr.koutroulis.chris.products.service;

import gr.koutroulis.chris.products.model.Customer;
import gr.koutroulis.chris.products.model.CustomerOrder;
import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.model.Stock;
import gr.koutroulis.chris.products.repository.CustomerRepository;
import gr.koutroulis.chris.products.repository.OrderRepository;
import gr.koutroulis.chris.products.repository.ProductRepository;
import gr.koutroulis.chris.products.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerOrderService {

    private OrderRepository orderRepository;
    private ProductRepository productRepository;
    private CustomerRepository customerRepository;
    private StockRepository stockRepository;

    @Autowired
    public CustomerOrderService(OrderRepository orderRepository, ProductRepository productRepository,
                                CustomerRepository customerRepository, StockRepository stockRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
        this.stockRepository = stockRepository;
    }


    public Optional<CustomerOrder> addOrder(CustomerOrder customerOrder) {        
        boolean foundUnknownCustomer = isCustomerNotExists(customerOrder.getCustomer());

        if (foundUnknownCustomer) {
            return Optional.empty();
        }

        CustomerOrder savedOrder = orderRepository.save(customerOrder);
        return Optional.ofNullable(savedOrder);
    }
    
    private boolean isCustomerNotExists(Customer customer) {
        return Optional.of(customer)
                .map(Customer::getId)
                .filter(customerId -> customerRepository.findById(customerId)
                        .isEmpty())
                .isPresent();
    }

    public Optional<CustomerOrder> findOrder(Long id) {
        return orderRepository.findById(id);
    }

    public CustomerOrder updateOrder(CustomerOrder customerOrder) {
        return orderRepository.save(customerOrder);
    }

    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }

    public List<CustomerOrder> getAllOrders() {
        List<CustomerOrder> customerOrderList = new LinkedList<>();
        orderRepository.findAll()
                .forEach(customerOrderList::add);

        return customerOrderList;
    }

    @Transactional
    public CustomerOrder updateOrderAndStock(CustomerOrder modifiedCustomerOrder) {
        List<Stock> stocksForOrderedProducts = findStocksForOrder(modifiedCustomerOrder);
        reduceAllQuantitiesByOne(stocksForOrderedProducts);

        CustomerOrder updatedOrder = updateOrder(modifiedCustomerOrder);

        return updatedOrder;
    }

    private List<Stock> findStocksForOrder(CustomerOrder foundCustomerOrder) {
        return foundCustomerOrder.getProductList().stream()
                .map(Product::getId)
                .map(productId -> stockRepository.findByProductId(productId))
                .collect(Collectors.toList());
    }

    private void reduceAllQuantitiesByOne(List<Stock> stockList) {
        stockList.stream()
                .forEach(stock -> {
                    Integer newQuantity = stock.getQuantity() - 1;
                    stock.setQuantity(newQuantity);
                    stockRepository.save(stock);
                });
    }
}
