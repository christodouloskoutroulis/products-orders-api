package gr.koutroulis.chris.products.service;


import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.model.Stock;
import gr.koutroulis.chris.products.repository.ProductRepository;
import gr.koutroulis.chris.products.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StockService {

    private StockRepository stockRepository;
    private ProductRepository productRepository;

    @Autowired
    public StockService(StockRepository stockRepository, ProductRepository productRepository) {
        this.stockRepository = stockRepository;
        this.productRepository = productRepository;
    }


    public Optional<Stock> addStock(Stock stock) {
        Product product = stock.getProduct();
        if (!isProductExists(product) || isStockExistsFor(product)) {
            return Optional.empty();
        }
        Stock savedStock = stockRepository.save(stock);
        return Optional.ofNullable(savedStock);
    }

    private boolean isStockExistsFor(Product product) {
        Long givenProductId = product.getId();
        return findStockForProductId(givenProductId)
                .isPresent();
    }

    public Optional<Stock> findStockForProductId(Long productId) {
        Stock foundStock = stockRepository.findByProductId(productId);
        return Optional.ofNullable(foundStock);
    }

    private boolean isProductExists(Product product) {
        return productRepository.findById(product.getId())
                .isPresent();
    }


    public Optional<Stock> findStock(Long id) {
        return stockRepository.findById(id);
    }

    public Stock updateStock(Stock stock) {
        return stockRepository.save(stock);
    }

    public void deleteStock(Long id) {
        stockRepository.deleteById(id);
    }

    public List<Stock> getAllStocks() {
        List<Stock> stockList = new LinkedList<>();
        stockRepository.findAll()
                .forEach(stockList::add);

        return stockList;
    }

    public boolean isAnyProductOutOfStock(List<Product> productList) {
        if (isAnyProductWithoutLinkedStock(productList)) {
            return true;
        }

        List<Stock> stockList = findStocksForProducts(productList);
        return isAnyStockQuantityZero(stockList);
    }

    private boolean isAnyProductWithoutLinkedStock(List<Product> productList) {
        return productList.stream()
                .map(Product::getId)
                .map(productId -> findStockForProductId(productId))
                .filter(stockOptional -> stockOptional.isEmpty())
                .findFirst()
                .isPresent();
    }

    private List<Stock> findStocksForProducts(List<Product> productList) {
        return productList.stream()
                .map(Product::getId)
                .map(productId -> findStockForProductId(productId))
                .flatMap(Optional::stream)
                .collect(Collectors.toList());
    }

    private boolean isAnyStockQuantityZero(List<Stock> stockList) {
        return stockList.stream()
                .map(Stock::getQuantity)
                .filter(quantity -> quantity == 0)
                .findFirst()
                .isPresent();
    }
}
