package gr.koutroulis.chris.products.controller;

import gr.koutroulis.chris.products.dto.ErrorResponseDTO;
import gr.koutroulis.chris.products.model.Stock;
import gr.koutroulis.chris.products.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/stocks")
public class StockController {

    private StockService stockService;

    @Autowired
    public StockController(StockService stockService) {
        this.stockService = stockService;
    }


    @RequestMapping("")
    public List<Stock> getStocks() {
        return stockService.getAllStocks();
    }


    @RequestMapping(method = RequestMethod.POST, value = "")
    public ResponseEntity<Object> addStock(@RequestBody Stock stock) {
        Optional<Stock> stockOptional = stockService.addStock(stock);

        if (stockOptional.isPresent()) {
            return ResponseEntity.created(null)
                    .body(stockOptional.get());
        } else {
            ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO("A Stock entry already exists for this product or the product does not exist.");
            return ResponseEntity.badRequest()
                    .body(errorResponseDTO);
        }
    }


    @RequestMapping("/query")
    public ResponseEntity<Object> findStockForProduct(@RequestParam(value = "productId", required = false) Long productId) {
        Optional<Stock> stockOptional = stockService.findStockForProductId(productId);

        if (stockOptional.isEmpty()) {
            return ResponseEntity.notFound()
                    .build();
        }

        return ResponseEntity.ok()
                .body(stockOptional.get());
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> updateStock(@RequestBody Stock stock, @PathVariable Long id) {
        Optional<Stock> foundStockOptional = stockService.findStock(id);
        if (foundStockOptional.isEmpty()) {
            return ResponseEntity.notFound()
                    .build();
        }

        stock.setId(id);
        Stock updatedStock = stockService.updateStock(stock);
        return ResponseEntity.ok()
                .body(updatedStock);
    }
}
