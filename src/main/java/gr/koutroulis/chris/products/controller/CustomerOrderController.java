package gr.koutroulis.chris.products.controller;

import gr.koutroulis.chris.products.dto.ErrorResponseDTO;
import gr.koutroulis.chris.products.dto.OrderStatusDTO;
import gr.koutroulis.chris.products.model.CustomerOrder;
import gr.koutroulis.chris.products.model.enums.OrderStatus;
import gr.koutroulis.chris.products.service.CustomerOrderService;
import gr.koutroulis.chris.products.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
public class CustomerOrderController {

    private CustomerOrderService customerOrderService;
    private StockService stockService;

    @Autowired
    public CustomerOrderController(CustomerOrderService customerOrderService, StockService stockService) {
        this.customerOrderService = customerOrderService;
        this.stockService = stockService;
    }


    @RequestMapping("")
    public List<CustomerOrder> getOrders() {
        return customerOrderService.getAllOrders();
    }


    @RequestMapping(method = RequestMethod.POST, value = "")
    public ResponseEntity<Object> addOrder(@RequestBody CustomerOrder order) {
        boolean isProductsOutOfStock = stockService.isAnyProductOutOfStock(order.getProductList());

        if (isProductsOutOfStock) {
            ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO("One or more products are unknown or out of stock. The order was not placed.");
            return ResponseEntity.badRequest()
                    .body(errorResponseDTO);
        }

        Optional<CustomerOrder> savedOrderOptional = customerOrderService.addOrder(order);

        if (savedOrderOptional.isPresent()) {
            return ResponseEntity.created(null)
                    .body(savedOrderOptional.get());
        } else {
            ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO("Unknown customer. The order was not placed.");
            return ResponseEntity.badRequest()
                    .body(errorResponseDTO);
        }
    }


    @RequestMapping(method = RequestMethod.PATCH, value = "{id}/status")
    public ResponseEntity<Object> changeOrderStatus(@RequestBody OrderStatusDTO orderStatusDTO,
                                                    @PathVariable Long id) {
        Optional<CustomerOrder> foundCustomerOrderOptional = customerOrderService.findOrder(id);

        if (foundCustomerOrderOptional.isEmpty()) {
            return ResponseEntity.notFound()
                    .build();
        }

        OrderStatus newStatus = orderStatusDTO.getStatus();
        CustomerOrder foundCustomerOrder = foundCustomerOrderOptional.get();
        foundCustomerOrder.setStatus(newStatus);

        CustomerOrder updatedCustomerOrder = null;
        if (OrderStatus.DELIVERED.equals(newStatus)) {
            updatedCustomerOrder = customerOrderService.updateOrderAndStock(foundCustomerOrder);

        } else {
            updatedCustomerOrder = customerOrderService.updateOrder(foundCustomerOrder);
        }

        return ResponseEntity.ok()
                .body(updatedCustomerOrder);

    }


}
