package gr.koutroulis.chris.products.controller;

import gr.koutroulis.chris.products.model.Customer;
import gr.koutroulis.chris.products.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    @RequestMapping("")
    public List<Customer> getCustomers() {
        return customerService.getAllCustomers();
    }
}
