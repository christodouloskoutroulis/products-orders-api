package gr.koutroulis.chris.products.controller;

import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @RequestMapping("")
    public List<Product> getProducts() {
        return productService.getAllProducts();
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    public ResponseEntity<Object> addProduct(@RequestBody Product product) {
        Product savedProduct = productService.addProduct(product);

        return ResponseEntity.created(null)
                .body(savedProduct);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable Long id) {
        Optional<Product> foundProductOptional = productService.findProduct(id);
        if (foundProductOptional.isEmpty()) {
            return ResponseEntity.notFound()
                    .build();
        }

        productService.deleteProduct(id);
        return ResponseEntity.noContent()
                .build();
    }

}
