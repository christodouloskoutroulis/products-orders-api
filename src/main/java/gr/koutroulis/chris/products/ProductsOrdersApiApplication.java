package gr.koutroulis.chris.products;

import gr.koutroulis.chris.products.util.DatabaseInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ProductsOrdersApiApplication {

    private final static Logger logger = LoggerFactory.getLogger(ProductsOrdersApiApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext appContext = SpringApplication.run(ProductsOrdersApiApplication.class, args);
        DatabaseInitializer databaseInitializer = appContext.getBean(DatabaseInitializer.class);
        databaseInitializer.initialize();

        logger.info("The application is ready.");
    }

}
