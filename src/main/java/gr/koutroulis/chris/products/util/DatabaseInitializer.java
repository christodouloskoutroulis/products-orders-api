package gr.koutroulis.chris.products.util;

import gr.koutroulis.chris.products.model.Customer;
import gr.koutroulis.chris.products.model.Product;
import gr.koutroulis.chris.products.model.Stock;
import gr.koutroulis.chris.products.model.enums.ProductType;
import gr.koutroulis.chris.products.service.CustomerService;
import gr.koutroulis.chris.products.service.ProductService;
import gr.koutroulis.chris.products.service.StockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseInitializer {

    private final static Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);

    private ProductService productService;
    private StockService stockService;
    private CustomerService customerService;

    public DatabaseInitializer(ProductService productService, StockService stockService, CustomerService customerService) {
        this.productService = productService;
        this.stockService = stockService;
        this.customerService = customerService;
    }

    public void initialize() {
        logger.info("Populating database with sample data.");
        
        List<Product> sampleProductList = createThreeSampleProducts();
        storeSampleProducts(sampleProductList);

        List<Stock> stockList = createStockForThreeProducts(sampleProductList);
        storeStocks(stockList);

        List<Customer> customerList = createCustomerList();
        storeCustomers(customerList);
    }


    private List<Product> createThreeSampleProducts() {
        List<Product> productList = new ArrayList<>();

        Product product1 = new Product();
        product1.setName("Smartphone");
        product1.setType(ProductType.DEVICE);
        product1.setDescription("The new flagship from Swisscom.");

        Product product2 = new Product();
        product2.setName("Business pack");
        product2.setType(ProductType.CONTRACT);
        product2.setDescription("All a businessman needs froma communication pack.");

        Product product3 = new Product();
        product3.setName("Ethernet cable");
        product3.setType(ProductType.MISCELLANEOUS);
        product3.setDescription("10m of ethernet cable.");

        productList.add(product1);
        productList.add(product2);
        productList.add(product3);

        return productList;
    }

    private void storeSampleProducts(List<Product> sampleProductList) {
        logger.info("Storing sample products.");        
        sampleProductList.stream()
                .forEach(productService::addProduct);
    }


    private List<Stock> createStockForThreeProducts(List<Product> sampleProductList) {
        List<Stock> stockList = new ArrayList<>();

        Stock stock1 = new Stock();
        stock1.setProduct(sampleProductList.get(0));
        stock1.setQuantity(100);

        Stock stock2 = new Stock();
        stock2.setProduct(sampleProductList.get(1));
        stock2.setQuantity(500);

        Stock stock3 = new Stock();
        stock3.setProduct(sampleProductList.get(2));
        stock3.setQuantity(800);

        stockList.add(stock1);
        stockList.add(stock2);
        stockList.add(stock3);

        return stockList;
    }

    private void storeStocks(List<Stock> stockList) {
        logger.info("Storing stock for sample products.");
        stockList.stream()
                .forEach(stockService::addStock);
    }


    private List<Customer> createCustomerList() {
        List<Customer> customerList = new ArrayList<>();

        Customer customer1 = new Customer();
        customer1.setName("Ben");
        customer1.setSurname("Hur");
        customer1.setAddress("Street 1");

        Customer customer2 = new Customer();
        customer2.setName("Michael");
        customer2.setSurname("Page");
        customer2.setAddress("Street 2");

        Customer customer3 = new Customer();
        customer3.setName("Jack");
        customer3.setSurname("Daniel");
        customer3.setAddress("Street 3");

        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);

        return customerList;
    }

    private void storeCustomers(List<Customer> customerList) {
        logger.info("Storing customers.");
        customerList.stream()
                .forEach(customerService::addCustomer);
    }
    
}
